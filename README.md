MEAN STACK
==========

This is the tutorial from the 2020 udemy course named
["Angular & Node JS - The MEAN Stack Guide"](https://www.udemy.com/course/angular-2-and-nodejs-the-practical-guide/learn/lecture/10577670#overview)

---

Current Course Status
-----------

- [x] Getting Started
- [x] The Angular Frontend - Understanding the Basics
- [x] Adding NodeJS to our Project
- [x] Working with MongoDB
- [x] Enhancing the App
- [x] Adding Image Uploads to our App
- [x] Adding Pagination
- [x] Adding User Authentication
- [x] Authorization
- [x] Handling Errors
- [x] Optimizations
- [ ] Deploying Our App

**NOTE**: Please do not push to this repository until the end of each section. 

Setup the MEAN Environment
=======

- [Install NodeJS](https://nodejs.org/en/download/ "NODEJS INSTALL")

- Install Angular CLI

  - `npm install -g @angular/cli`

- Install Angular Material

  - `ng add @angular/material`

- Install Body Parser for Posts

  -  `npm install --save body-parser`

- Install Express

  - `npm install --save express`

- Install Nodemon for Server side refresh on save

    1. `npm install --save-dev nodemon`
    2. Go to package.json and set script starter name
    3. Example: `"start:server": "nodemon server.js"`
    4. NOTE: To start the server side, do `npm run start:server`  

  

- [Install MongoDB](https://www.mongodb.com/)

    + You can install MongoDB package with `npm install --save mongodb`  
    + Or install Mongoose with `npm install --save mongoose` (Used in this course)  
  
  

- [Install MongoShell (Link to installer)](https://downloads.mongodb.org/win32/mongodb-shell-win32-x86_64-2012plus-4.2.3.zip)

  1. Extract the zip to desired location
  2. NOTE: If you would like to be able to just type in 'mongo' as a command, then add your mongo bin folder to your PATH environment variable.  

  

- Install Multer for file upload on the server side

  - `npm install --save multer`


- Install Mongoose Unique Validator

  - `npm install --save mongoose-unique-validator`

- Install Bcrypt for hashing

  - `npm install --save bcrypt` 

- Install json token creator

  - `npm install --save jsonwebtoken`


Known Bugs
=======

- Whenever you delete a post and you go back to the post-list.component.html then it will show no data until you refresh
- The Error Dialog 'Okay' button does not. None of the directives actually seem to work.
