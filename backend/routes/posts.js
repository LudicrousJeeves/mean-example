const express = require("express");

const checkAuth = require("../utl/check-auth");
const extractFile = require("../utl/file");
const PostsController = require("../controllers/posts");

const router = express.Router();

//Create a new post
router.post("", checkAuth, extractFile, PostsController.createPost);

//Get posts according to the values set with pagination
router.get("", PostsController.getPosts);

//Updates the selected post
router.put("/:id", checkAuth, extractFile, PostsController.updatePost);

//Get post by ID
router.get("/:id", PostsController.getPostById);

//Deletes selected post
router.delete("/:id", checkAuth, PostsController.deletePost);

module.exports = router;
